<?php

namespace App\Console\Commands;

use App\Models\{Country,Town,County,Property,Room};
use Curl\Curl;
use Illuminate\Console\Command;

class CopyRemoteRooms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy-remote-rooms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save remote rooms data to DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $page = 0;
        $perPage = 100;
        $totalPages = 1;

        while ($page <= $totalPages) {
            $page++;
            $curl = new Curl();
            $curl->setHeader('X-Requested-With', 'XMLHttpRequest');
            $curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE);
            $curl->setUserAgent('');
            $curl->get(config('constants.API_URL') . '/properties', [
//                'api_key'   => config('constants.API_KEY'),
                'page'  => [
                    'num'   => $page,
                    'size'  => $perPage
                ]
            ]);
            $curl->close();

            if($curl->error) {
                $this->error("Page $page / $totalPages - Fail");
                continue;
            }

            $res = (object) json_decode($curl->response, true);
            $totalPages = $res->last_page;
            $rooms = $res->data;

            foreach ($rooms as $rroom) {
                $rroom = (object) $rroom;
                $rproperty = (object) $rroom->property_type;
                $type = $rroom->type == 'rent' ? Room::FOR_RENT : Room::FOR_SALE;
                $country = Country::firstOrCreate([
                    'name'  => (string) $rroom->country
                ]);
                $town = Town::firstOrCreate(
                    [
                        'name'  => (string) $rroom->town
                    ],
                    [
                        'country_id'   => $country->id
                    ]
                );
                $county = County::firstOrCreate(
                    [
                        'name'  => (string) $rroom->county
                    ],
                    [
                        'town_id'   => (int) $town->id
                    ]
                );
                $property = Property::firstOrCreate(
                    [
                        'ext_id'    => (int) $rproperty->id
                    ],
                    [
                        'name'          => (string) $rproperty->title,
                        'description'   => (string) $rproperty->description,
                    ]
                );
                Room::firstOrCreate(
                    [
                        'ext_id'    => $rroom->uuid,
                    ],
                    [
                        'address'       => (string) $rroom->address,
                        'description'   => (string) $rroom->description,
                        'image'         => (string) $rroom->image_full,
                        'thumbnail'     => (string) $rroom->image_thumbnail,
                        'latitude'      => (float) $rroom->latitude,
                        'longitude'     => (float) $rroom->longitude,
                        'nb_bedrooms'   => (int) $rroom->num_bedrooms,
                        'nb_bathrooms'  => (int) $rroom->num_bathrooms,
                        'price'         => (float) $rroom->price,
                        'type'          => $type,
                        'property_id'   => $property->id,
                        'county_id'     => $county->id
                    ]
                );
            }

            $this->info("Page $page / $totalPages - Ok");
        }

        return 0;
    }
}
