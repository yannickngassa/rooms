<?php

namespace App\Http\Controllers;

use App\Http\Resources\TownResource;
use App\Models\Town;

class TownController extends Controller
{
    function index(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return TownResource::collection(Town::all());
    }
}
