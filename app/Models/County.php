<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static firstOrCreate(string[] $array, int[] $array1)
 */
class County extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'name',
        'town_id'
    ];

    /**
     * Get the town of the county
     *
     * @return BelongsTo
     */
    public function town(): BelongsTo
    {
        return $this->belongsTo(Town::class);
    }
}
