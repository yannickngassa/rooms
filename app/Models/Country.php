<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\{HasMany,HasManyThrough};

/**
 * @method static firstOrCreate(string[] $array)
 */
class Country extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    /**
     * Get all towns of the country
     *
     * @return HasMany
     */
    function towns(): HasMany
    {
        return $this->hasMany(Town::class);
    }

    /**
     * Get all cities of the country
     *
     * @return HasManyThrough
     */
    public function counties(): HasManyThrough
    {
        return $this->hasManyThrough(County::class, Town::class);
    }
}
