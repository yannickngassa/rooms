<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @method static when(mixed $type, \Closure $param)
 * @method static firstOrCreate(array $array, array $array1)
 */
class Room extends Model
{
    use HasFactory;
    use SoftDeletes;

    public const FOR_SALE = 1;
    public const FOR_RENT = 2;

    protected $attributes = [
        'type' => self::FOR_RENT,
    ];
    protected $casts = [
    ];
    protected $fillable = [
        'ext_id',
        'address',
        'description',
        'latitude',
        'longitude',
        'image',
        'thumbnail',
        'price',
        'nb_bedrooms',
        'nb_bathrooms',
        'property_id',
        'county_id',
        'type',
    ];

    function setTypeAttribute(int $type): void
    {
        $this->attributes['type'] = match ($type) {
            self::FOR_RENT, self::FOR_SALE => $type,
            default => throw new \ValueError(),
        };
    }

    /**
     * Get the county of the room
     *
     * @return BelongsTo
     */
    function county(): BelongsTo
    {
        return $this->belongsTo(County::class);
    }

    /**
     * Get the property of the room
     *
     * @return BelongsTo
     */
    function property(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }

    #[ArrayShape([self::FOR_RENT => "string", self::FOR_SALE => "string"])] static function getTypes(): array
    {
        return [
            Room::FOR_RENT  => 'Rent',
            Room::FOR_SALE  => 'Sale'
        ];
    }
}
