<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->uuid('ext_id');
            $table->string('address')
                ->nullable();
            $table->text('description')
                ->nullable();
            $table->string('image')
                ->nullable();
            $table->string('thumbnail')
                ->nullable();
            $table->float('latitude');
            $table->float('longitude');
            $table->smallInteger('nb_bedrooms')
                ->nullable()
                ->unsigned();
            $table->smallInteger('nb_bathrooms')
                ->nullable()
                ->unsigned();
            $table->integer('price')
                ->unsigned();
            $table->smallInteger('type')
                ->unsigned();
            $table->foreignIdFor(\App\Models\County::class)
                ->constrained()
                ->cascadeOnDelete();
            $table->foreignIdFor(\App\Models\Property::class)
                ->constrained()
                ->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
};
