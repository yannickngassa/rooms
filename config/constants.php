<?php
return [
    'API_URL'   => env('API_URL'),
    'API_KEY'   => env('API_KEY')
];
