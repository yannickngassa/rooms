import type { InjectionKey } from 'vue'
import { createStore, Store } from 'vuex'

export interface State {
  rooms: {
    filter: {
      formData: Record<string, any>
    }
  }
}

export const key: InjectionKey<Store<State>> = Symbol()

const store = createStore<State>({
  state () {
    return {
      rooms: {
        filter: {
          formData: {
            page: 1
          }
        }
      },
    }
  },
  getters: {
    roomsFilterParams(state) {
      return state.rooms.filter.formData;
    }
  },
  mutations: {
    updateRoomFilterData(state, payload) {
      state.rooms.filter.formData = {
        ...state.rooms.filter.formData,
        ...payload
      };
    }
  },
});

export default store;
