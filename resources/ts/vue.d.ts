import type Vue from 'vue'
import type { Store } from 'vuex'
import type { State } from './store';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: Store<State>
  }
}

declare module '*.vue' {
  export default Vue
}
