import { createApp } from "vue"
// @ts-ignore
import FilterComponent from "./FilterComponent"
// @ts-ignore
import RoomCollection from './RoomCollectionComponent'
import store, { key } from './store';

createApp(FilterComponent)
  .use(store, key)
  .mount('#filterContainer');

createApp(RoomCollection)
  .use(store, key)
  .mount('#roomCollectionContainer');
