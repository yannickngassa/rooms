<div class="rooms-filter">
    {{ Form::open([
        'url'       => '.',
        'method'    => 'get'
    ]) }}
        <div class="mb-3">
            {{ Form::label('town', 'Town', [
                'class' => 'form-label'
            ]) }}
            {{ Form::select('town[]', $filter['towns'], $filter_defaults['towns'], [
                'class'     => 'form-control',
                'multiple'  => 'multiple'
            ]) }}
        </div>
        <div class="mb-3">
            {{ Form::label('nb_bedrooms', 'Nb. of bedrooms', [
                'class' => 'form-label'
            ]) }}
            {{ Form::number('nb_bedrooms', $filter_defaults['nb_bedrooms'], [
                'min'   => 1,
                'class' => 'form-control'
            ]) }}
        </div>
        <div class="mb-3">
            {{ Form::label('price[from]', 'Price', [
                'class' => 'form-label'
            ]) }}
            {{ Form::number('price[from]', $filter_defaults['price']['from'], [
                'min'   => 1,
                'class' => 'form-control'
            ]) }}
            {{ Form::number('price[to]', $filter_defaults['price']['to'], [
                'min'   => $filter_defaults['price']['from'] ?? 1,
                'class' => 'form-control'
            ]) }}
        </div>
        <div class="mb-3">
            {{ Form::label('type', 'Property type', [
                'class' => 'form-label'
            ]) }}
            {{ Form::select('type', $filter['types'], $filter_defaults['type'], [
                'class' => 'form-control'
            ]) }}
        </div>
        <button type="submit" class="btn btn-primary">Filter</button>
        <button type="reset" class="btn btn-outline-primary">Reset</button>
    {{ Form::close() }}
</div>
