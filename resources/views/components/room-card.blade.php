<div class="card room-card">
    <img src="{{ $room->thumbnail }}" alt="room" class="card-img-top" loading="lazy" />
    <div class="card-body">
        <h5 class="card-title">Room {{ $room->id }}</h5>
        <div class="card-text">{{ $room->description }}</div>
    </div>
</div>
