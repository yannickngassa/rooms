<div class="rooms pb-4">
    <div class="row row-cols-2 row-cols-md-4 row-cols-xl-3 align-items-stretch">
        @foreach($rooms as $room)
            <div class="rooms__col">
                @include('components.room-card', [
                    'room'  => $room
                ])
            </div>
        @endforeach
    </div>
</div>

{{ $rooms->onEachSide(2)->links() }}
