<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rooms</title>
        <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    </head>
    <body>
        <div class="container">
            <h1>Rooms</h1>
            <div class="row">
                <div class="col-md-3" id="filterContainer"></div>
                <div class="col-md-9" id="roomCollectionContainer">
                </div>
            </div>
        </div>
    <script src="{{asset('/js/app.js')}}"></script>
    </body>
</html>
